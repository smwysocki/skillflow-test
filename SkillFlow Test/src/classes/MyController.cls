public with sharing class MyController{
    private final Resume__c resume = [SELECT Name, Last_Updated__c, First_Name__c, Middle_Name__c, Last_Name__c, Primary_Phone__c, Primary_Email__c, Agency_Holding_Clearance__c, Clearance__c, Polygraph__c FROM Resume__c
                   WHERE Identifier__c = :UserInfo.getUserId()];
    
    public List<Skill_Name__c> skill {get;set;}//List of My Skills
    List<Education__c> Edu;//List of my Educations
    List<Certification__c> Cert;//List of my Certifications
    List<Project__c> project;//List of my projects
    public Boolean displayPopup {get;set;}//if pop up to edit fields is visable
    public String SearchString {get;set;}//for homepage to search page query string
    public Boolean ApprovalUpdate {
        get{
            if(ApprovalUpdate==null){
                ApprovalUpdate=false;
            }
            return ApprovalUpdate;
        }
        set;
    }
    
    //Queries the classifications of the user to see if the user has access to the page.
    public ApexPages.StandardSetController setclassification{
        get {
            if(setclassification== null) {
                setclassification= new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT User__c, Billable_Employee__c, Human_Resources__c, Bussiness_Development__c, System_Administrator__c, Executive__c FROM Classification__c WHERE User__c = :UserInfo.getUserId()]));
            }
            return setclassification;
        }
        set;
    }
    
    //retrieves the classifications of the user to see if the user has access to the page.
    public Classification__c getClassifications() {
        Classification__c MyClassifications = new Classification__c();
        for(Classification__c temp : (List<Classification__c>) setclassification.getRecords()){
            MyClassifications.Billable_Employee__c = temp.Billable_Employee__c;
            MyClassifications.Human_Resources__c = temp.Human_Resources__c;
            MyClassifications.Bussiness_Development__c = temp.Bussiness_Development__c;
            MyClassifications.System_Administrator__c = temp.System_Administrator__c;
            MyClassifications.Executive__c = temp.Executive__c ;
        }
        return MyClassifications;
    }
    
    //shows the edit pop for projects section
    public void showPopup(){
        displayPopup = true;
    }
    
    //hides the edit popup for the projects section
    public void closePopup(){
        displayPopup = false;
    }
    
    //gives an initial values of skill
    public MyController() {
        Skill = getSkills();
    }

    //initiates the workflow to notify the user every 90days
    public void UpdateDate(){
    
        //Cancel 90 Day workflow
        Date myDate = Date.parse('01/1/2001');
        resume.Last_Updated__c = myDate;
        update resume;
        
        //Update to today and start 90 day workflow
        resume.Last_Updated__c = today();
        update resume;
    }
    
    //queries my skills
    public ApexPages.StandardSetController setSkill {
        get {
            if(setSkill == null) {
                setSkill = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Name, Delete__c, Skill_Level__c, Resume__c, SkillName__r.Name__c, SkillName__r.SkillCategoryName__r.Name__c FROM Skill_name__c WHERE Resume__r.Identifier__c = :UserInfo.getUserId() ORDER BY SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c]));
            }
            return setSkill;
        }
        set;
    }
    
    //queries my skills
    public List<Skill_name__c> getSkills() {
        Skill = [SELECT Name, Delete__c, Skill_Level__c, Resume__c, SkillName__r.Name__c, SkillName__r.SkillCategoryName__r.Name__c FROM Skill_name__c WHERE Resume__r.Identifier__c = :UserInfo.getUserId() ORDER BY SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c ASC];
        return Skill;
    }
    
    //queries my Educations
    public ApexPages.StandardSetController setEdu {
        get {
            if(setEdu == null) {
                setEdu = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Delete__c, City__c, State__c, Degree_Recieved__c, Field_Of_Study__c, Graduation_Date__c, School__c FROM Education__c WHERE Resume__r.Identifier__c = :UserInfo.getUserId() ORDER BY Graduation_Date__c ASC]));
            }
            return setEdu;
        }
        set;
    }
    
    //queries my Certifications
    public ApexPages.StandardSetController setCert {
        get {
            if(setCert == null) {
                setCert = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Delete__c, CertificationName__c, Certification_Name__c, Certification_Id__c, Date_Received__c, CertificationName__r.Name__c FROM Certification__c cert WHERE Resume__r.Identifier__c = :UserInfo.getUserId() ORDER BY Date_Received__c ASC]));
            }
            return setCert;
        }
        set;
    }

    //queries my Projects
    public ApexPages.StandardSetController setProject {
        get {
            if(setProject == null) {
                setProject = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Delete__c, Company__c, Current_Position__c, Project_Name__c, Position_Title__c, Project_Start__c, End_Date__c, Description__c FROM Project__c WHERE Resume__r.Identifier__c = :UserInfo.getUserId() ORDER BY Project_Start__c DESC]));
            }
            return setProject;
        }
        set;
    }
   
    //retrieves the degrees for a selectList from the object picklist
    public List<SelectOption> getDegreeRecievedList(){
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Education__c.Degree_Recieved__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry temp : ple)
            {
                    options.add(new SelectOption(temp.getLabel(), temp.getValue()));
            }
            return options;
    }
    
    
    public String SelectedNewSkillName {get;set;} //the Skill Selected when adding a new skill
    public String SelectedNewSkillCategoryName {get;set;} // the skill category selected when adding a new skill 
    
    //the skill names available to be selected according the the skillcategory it is given
    public List<SelectOption> SkillNames{
        get{
            List<SelectOption> options = new List<SelectOption>();
            if(SelectedNewSkillCategoryName != null){
                options.add(new SelectOption('', '---Choose A Skill---'));
                if(SelectedNewSkillCategoryName != 'Other'){
                    List<SkillName__c> names = new List<SkillName__c>();
                    names = [SELECT Name__c, Name, SkillCategoryName__r.Name__c FROM SkillName__c Order By Name__c];
                    for(SkillName__c temp : names)
                    {
                        if(SelectedNewSkillCategoryName == temp.SkillCategoryName__r.id)
                            options.add(new SelectOption(temp.id, temp.Name__c));
                    }
                    options.add(new SelectOption('Other', 'Other'));
                }else{
                    options.add(new SelectOption('Other', 'Other'));
                }
            }
            return options;
            
        }
        set;
    }
    
    //holds the List of skills for each skill Category
    public List<Tuple> MySkillList{get; set;}

    //the list of skills skills that the user has
    public List<Tuple> getMySkills(){
        if(MySkillList==Null){
            MySkillList = new List<Tuple>();
            for(SkillCategoryName__c temp : [SELECT Name__c FROM SkillCategoryName__c]){
                List<Skill_Name__c> count = [Select id from Skill_Name__c WHERE SkillName__r.SkillCategoryName__r.Name__c =:temp.Name__c AND Resume__c= :resume.id];
                if(count.size() > 0){
                    MySkillList.add(new Tuple(temp.Name__c, [SELECT SkillName__r.Name__c, Skill_Level__c FROM Skill_Name__c Where Resume__c= :resume.id AND SkillName__r.SkillCategoryName__r.Name__c=:temp.Name__c Order By SkillName__r.Name__c]));
                }
            }
        }
        return MySkillList;
    }
    
    //the list of all skills 
    public List<SelectOption> getEditSkillNames(){
        
            List<SelectOption> options = new List<SelectOption>();
                    for(SkillName__c temp : [SELECT Name__c, Name, SkillCategoryName__r.Name__c FROM SkillName__c Order By Name__c])
                    {
                            options.add(new SelectOption(temp.id, temp.Name__c));
                    }
                    options.add(new SelectOption('Other', 'Other'));
            return options;
    }
    
    //the list of all skill categoryies when editing a skill ** currently not used
    public List<SelectOption> getEditSkillCategoryNames(){
        
            List<SelectOption> options = new List<SelectOption>();
            for(SkillCategoryName__c temp : [SELECT Name__c, Name FROM SkillCategoryName__c Order By Name__c])
            {
                    options.add(new SelectOption(temp.id, temp.Name__c));
            }
            options.add(new SelectOption('Other', 'Other'));
            return options;
        
    }
    
    //the list of skill categories when creating a new skill
    public List<SelectOption> getSkillCategoryNames(){
        
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('', '---Choose A Category---'));
            List<SkillCategoryName__c> names = new List<SkillCategoryName__c>();
            names = [SELECT Name__c, Name FROM SkillCategoryName__c Order By Name__c];
            for(SkillCategoryName__c temp : names)
            {
                    options.add(new SelectOption(temp.id, temp.Name__c));
            }
            options.add(new SelectOption('Other', 'Other'));
            return options;
        
    }
    
    //List of all Certification names when adding a new certification
    public List<SelectOption> CertNames{
        get{
            List<SelectOption> options = new List<SelectOption>();
            List<Certification_Name__c> names = new List<Certification_Name__c>();
            names = [SELECT Name__c, Name FROM Certification_Name__c Order By Name__c];
            for(Certification_Name__c temp : names)
            {
                    options.add(new SelectOption(temp.id, temp.Name__c));
            }
            options.add(new SelectOption('Other', 'Other'));
            return options;
        }
    }
    
    //New SkillCertificationName object for when the user chooses other
    public SkillCategoryName__c NewOtherSkillCategoryName{
        get{
            if (NewOtherSkillCategoryName== null)
                NewOtherSkillCategoryName= new SkillCategoryName__c();
            return NewOtherSkillCategoryName;
        }
        set;
    }

    //New SkillName object for when the user chooses other
    public SkillName__c NewOtherSkillName{
        get{
            if (NewOtherSkillName== null)
                NewOtherSkillName= new SkillName__c();
            return NewOtherSkillName;
        }
        set;
    }
    
    //New SkillName for when the user chooses the default values from the select list
    public SkillName__c NewSkillName{
        get {
          if (NewSkillName== null)
            NewSkillName = new SkillName__c();
          return NewSkillName;
        }
        set;
    }
    
    //new skillCategory object for when the user chooses a default value from the skillcategory select list
    public SkillCategoryName__c NewSkillCategoryName{
        get {
          if (NewSkillCategoryName== null)
            NewSkillCategoryName= new SkillCategoryName__c();
          return NewSkillCategoryName;
        }
        set;
    }
    
    //new Skill object for when the user want to edit their skill ** not currently in use
    public Skill_Name__c NewEditSkill{
        get {
          if (NewSkill == null)
            NewSkill = new Skill_Name__c();
            NewSkill.Resume__c = resume.id;
          return NewSkill;
        }
        set;
    }
    
    //New Skill for when adding a new skill record to the resume
    public Skill_Name__c NewSkill{
        get {
          if (NewSkill == null)
            NewSkill = new Skill_Name__c();
            NewSkill.Resume__c = resume.id;
          return NewSkill;
        }
        set;
    }
    
    //New Education for when adding a new Education record to the resume
    public Education__c NewEdu{
        get {
          if (NewEdu == null)
            NewEdu = new Education__c ();
            NewEdu.Resume__c = resume.id;
          return NewEdu;
        }
        set;
    }
    
    //New Certificationfor when adding a new Certification record to the resume
    public Certification__c NewCert{
        get {
          if (NewCert == null)
            NewCert= new Certification__c ();
            NewCert.Resume__c = resume.id;
          return NewCert;
        }
        set;
    }
    
    //New Certification Name for when the user enters a new Certification
    public Certification_Name__c NewCertName{
        get {
          if (NewCertName == null)
            NewCertName = new Certification_Name__c ();
          return NewCertName;
        }
        set;
    }
    
    //New Certification for when adding a new Certification record to the resume and selects other
    public Certification_Name__c NewOtherCertName{
        get {
          if (NewOtherCertName == null)
            NewOtherCertName = new Certification_Name__c();
          return NewOtherCertName;
        }
        set;
    }
    
    //New Project for when adding a new Project record to the resume
    public Project__c NewProject{
        get {
          if (NewProject== null)
            NewProject= new Project__c ();
            NewProject.Resume__c = resume.id;
          return NewProject;
        }
        set;
    }
    
    //retrieves the Educations that the user has
    public List<Education__c> getEdu() {
        return (List<Education__c>) setEdu.getRecords();
    }

    //retrieves the Certifications that the user has
    public List<Certification__c> getCert() {
        return (List<Certification__c>) setCert.getRecords();
    }
    
    //retrieves the Projectsthat the user has
    public List<Project__c> getProject() {
        return (List<Project__c>) setProject.getRecords();
    }
    
    
    //retrieves the resume general information that belongs to the user
    public Resume__c getResume() {
        return resume;
    }

    //returns todays date
    public Date today(){
        Date d;
        d = Date.today();
        return d;
    }
    
    //creates a new audit for the audit table
    public void audit(String NewValue, String table){
        Audits__c audit = new audits__c();
        audit.NewValue__c = NewValue;
        audit.DateChanged__c = today();
        audit.Editor__c = UserInfo.getUserId();
        audit.UserEdited__c = UserInfo.getUserId();
        audit.Archive__c = false;
        audit.ActionTaken__c = 'No Action Taken';
        audit.TableEdited__c = table;
        insert audit;
    }
    
    //adds a new skill to the users resume
    public PageReference saveSkill() {
    
        if(SelectedNewSkillCategoryName == 'Other'){//if other is selected for the SkillCategory
            NewSkillCategoryName.Name__c = NewOtherSkillCategoryName.Name__c;
            insert NewSkillCategoryName;
            
            NewSkillName.Name__c = NewOtherSkillName.Name__c;
            SkillCategoryName__c tempCat = [Select id from SkillCategoryName__c Where Name__c = :NewOtherSkillCategoryName.Name__c];
            NewSkillName.SkillCategoryName__c = tempCat.id;
            insert NewSkillName;
            
            SkillName__c tempSkill = [Select id, Name__c from SkillName__c Where Name__c = :NewOtherSkillName.Name__c AND SkillCategoryName__c =: tempCat.id];
            NewSkill.SkillName__c = tempSkill.id;
            audit(tempSkill.Name__c, 'Skill_Name__c');
            insert NewSkill;
            
        } else if(SelectedNewSkillName == 'Other'){//If Other is selected for the SkillName but not the skill category
            NewSkillName.Name__c = NewOtherSkillName.Name__c;
            NewSkillName.SkillCategoryName__c = SelectedNewSkillCategoryName;
            insert NewSkillName;
            
            SkillName__c tempSkill = [Select id, Name__c from SkillName__c Where Name__c = :NewOtherSkillName.Name__c AND SkillCategoryName__c =: SelectedNewSkillCategoryName];
            NewSkill.SkillName__c = tempSkill.id;
            audit(tempSkill.Name__c, 'Skill_Name__c');
            insert NewSkill;
        }else{//if only default values are selected to be added to the users resume
            NewSkill.SkillName__c = SelectedNewSkillName;
            insert NewSkill;
        }
        UpdateDate();
        
        //refresh the page
        pagereference mypage= new PageReference('/apex/MyResume?ctab=Skills');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //add a new education to the resume
    public PageReference saveEdu() {
        insert NewEdu;
        UpdateDate();
        pagereference redirect = new PageReference('/apex/MyResume?ctab=Education');
        redirect.setRedirect(true);
        system.debug('Hello@@'+redirect);
        return redirect;
    }
    
    //the value selected from the Certification dropdown
    public String SelectedCertName {get;set;}
    
    //add the new Certification the the user's resume
    public PageReference saveCert() {
        if(SelectedCertName =='Other'){//if Certification Name is other
            NewCertName.Name__c = NewOtherCertName.Name__c;
            insert NewCertName;
            
            Certification_Name__c tempCert = [Select id, Name__c from Certification_Name__c Where Name__c = :NewOtherCertName.Name__c];
            NewCert.CertificationName__c = tempCert.id;
            audit(tempCert.Name__c, 'Certification__c');
            insert NewCert;
        }else{//if only a default Certification name is selected
        
            NewCert.CertificationName__c = SelectedCertName;
            insert NewCert;
        }
        UpdateDate();
        pagereference redirect = new PageReference('/apex/MyResume?ctab=Education');
        redirect.setRedirect(true);
        system.debug('Hello@@'+redirect);
        return redirect;
    }
    
    //add the new project to the resume
    public PageReference saveProject() {
        insert NewProject;
        UpdateDate();
        pagereference redirect = new PageReference('/apex/MyResume?ctab=Projects');
        redirect.setRedirect(true);
        system.debug('Hello@@'+redirect);
        return redirect;
    }
    
    //Update the User's general information
    public PageReference EditGeneralInfo() {
        displayPopup = false;
        update resume;
        UpdateDate();
        pagereference mypage= new PageReference('/apex/MyResume?ctab=Skills');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //edit the project record using the popup
    public PageReference EditProject() {
        displayPopup = false;
        update getProject();
        UpdateDate();
        pagereference mypage= new PageReference('/apex/MyResume?ctab=Projects');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //delete all skills the user marked
    public PageReference DeleteSkill() {
        update Skill;
        UpdateDate();
        List<Skill_Name__c> deleteList = [SELECT id FROM Skill_Name__c WHERE Delete__c = True];
        for (Skill_Name__c DeleteSkill : deleteList){
            delete DeleteSkill;
        }
        pagereference mypage= new PageReference('/apex/MyResume?ctab=Skills');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //delete all educations that the user just marked
    public PageReference DeleteEducation() {
        update getEdu();
        UpdateDate();
        List<Education__c> deleteList = [SELECT id FROM Education__c WHERE Delete__c = True];
        for (Education__c DeleteEdu : deleteList){
            delete DeleteEdu;
        }
        pagereference mypage= new PageReference('/apex/MyResume?ctab=Education');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //Delete all certifications that the user just checked
    public PageReference DeleteCertification() {
        update getCert();
        UpdateDate();
        List<Certification__c> deleteList = [SELECT id FROM Certification__c WHERE Delete__c = True];
        for (Certification__c DeleteCert : deleteList){
            delete DeleteCert;
        }
        pagereference mypage= new PageReference('/apex/MyResume?ctab=Education');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //delete all projects that the user just checked
    public PageReference DeleteProject() {
        update getProject();
        UpdateDate();
        List<Project__c> deleteList = [SELECT id FROM Project__c WHERE Delete__c = True];
        for (Project__c DeleteProject : deleteList){
            delete DeleteProject;
        }
        pagereference mypage= new PageReference('/apex/MyResume?ctab=Projects');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //view the User's Corporate resume
    public PageReference ViewResume() {
        pagereference mypage= new PageReference('/apex/Corporate_Resume');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //for the home page, go to search page with the SearchString in the textbox
    public PageReference SearchKeyword() {
        pagereference mypage= new PageReference('/apex/Search?search='+SearchString);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //go to the Search page
    public PageReference Search() {
        pagereference mypage= new PageReference('/apex/Search');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //go to the resume buider for yourself
    public PageReference EditResume() {
        pagereference mypage= new PageReference('/apex/MyResume');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //go to the Modify Classification page
    public PageReference ModifyClassification() {
        pagereference mypage= new PageReference('/apex/ModifyClassifications');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //go to the audits page
    public PageReference Audits() {
        pagereference mypage= new PageReference('/apex/All_Audits');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //go to the Gap Analysis page
    public PageReference GapAnalysis() {
        pagereference mypage= new PageReference('/apex/Gap_Analysis');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //go to the New User page
    public PageReference NewUser() {
        pagereference mypage= new PageReference('/apex/New_User');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //go to the New User page
    public PageReference ResumeUpdateNoChanges() {
        if(ApprovalUpdate){
            UpdateDate();
        }
        return null;
    }
    
    //Tuple class used to pair Skill Category Name and the list of skills
    public class Tuple{
        public String value1 {get;set;}
        public List<Skill_Name__c> value2 {get;set;}
        
        public Tuple(String val1, List<Skill_Name__c> val2){
            value1 = val1;
            value2 = val2;
        }
    }
}