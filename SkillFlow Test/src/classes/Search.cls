public with sharing class Search{
    
    //String keyword used to search records - if 'Everyone', it pulls everyone
    public String SearchKeyword{
        get{
            if(SearchKeyword == null){
                //SearchKeyword = ApexPages.currentPage().getParameters().get('Search');
                if(ApexPages.currentPage().getParameters().get('Search') != null){
                    SearchKeyword = ApexPages.currentPage().getParameters().get('Search');
                } else {
                    SearchKeyword = 'Everyone';
                }
            }
            return SearchKeyword;
        }
        set;
    }
    
    //filter by firstname or lastname?
    public String filter = 'First Name';
    
    public ApexPages.StandardSetController setclassification{
        get {
            if(setclassification== null) {
                setclassification= new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT User__c, Billable_Employee__c, Human_Resources__c, Bussiness_Development__c, System_Administrator__c, Executive__c FROM Classification__c WHERE User__c = :UserInfo.getUserId()]));
            }
            return setclassification;
        }
        set;
    }
    
    public Classification__c getClassifications() {
        Classification__c MyClassifications = new Classification__c();
        for(Classification__c temp : (List<Classification__c>) setclassification.getRecords()){
            MyClassifications.Billable_Employee__c = temp.Billable_Employee__c;
            MyClassifications.Human_Resources__c = temp.Human_Resources__c;
            MyClassifications.Bussiness_Development__c = temp.Bussiness_Development__c;
            MyClassifications.System_Administrator__c = temp.System_Administrator__c;
            MyClassifications.Executive__c = temp.Executive__c ;
        }
        return MyClassifications;
    }
    
    public Search(){
    //Main Method that we do not need to do anything
    }
    // the collection of resumes to display
    List<Resume__c> people;//Variable that we will use to collect and display the search results
    
    //queries filtering by the search keyword
    public ApexPages.StandardSetController setpeople {
        get {
            String NewSearchKeyword = '%'+SearchKeyword+'%';//The % will be needed when comparing if the SearchKeyword exists in any records
            List<Skill_Name__c> skill = [Select Resume__c From Skill_Name__c WHERE SkillName__r.Name__c LIKE :NewSearchKeyword];//search for skills
            String[] SkillList = new String[skill.size()];
            Integer a = 0;
            for (Skill_Name__c temp : skill){
                SkillList[a] = temp.Resume__c;
                a++;
            }
            
            List<Education__c> Edu = [Select Resume__c From Education__c WHERE Field_Of_Study__c LIKE :NewSearchKeyword OR School__c Like :NewSearchKeyword];//search for Educations
            String[] EduList = new String[Edu.size()];
            a = 0;
            for (Education__c temp : Edu){
                EduList[a] = temp.Resume__c;
                a++;
            }
            
            List<Certification__c> Cert = [Select Resume__c From Certification__c WHERE Certification_Name__c LIKE :NewSearchKeyword];//Search for Certifications
            String[] CertList = new String[Cert.size()];
            a = 0;
            for (Certification__c temp : Cert){
                CertList[a] = temp.Resume__c;
                a++;
            }
            
            if((SearchKeyword == '' || SearchKeyword == 'Everyone') && filter=='First Name') {//default search or when searching with nothing in the search field
                setpeople = new ApexPages.StandardSetController(Database.getQueryLocator(//default Everyone search by first name
                    [SELECT Identifier__c, Searchable__c, First_Name__c, Middle_Name__c, Last_Name__c, (Select SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c From Skills__r order by SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c), (Select Degree_Recieved__c, Field_Of_Study__c, School__c From Educations__r order by Graduation_Date__c), (Select CertificationName__r.Name__c From Certifications__r order by CertificationName__r.Name__c) FROM Resume__c Where Searchable__c = true Order By First_Name__c]));
            } else if((SearchKeyword == '' || SearchKeyword == 'Everyone') && filter=='Last Name') {//default search or when searching with nothing in the search field
                setpeople = new ApexPages.StandardSetController(Database.getQueryLocator(//default everyone search by last name
                    [SELECT Identifier__c, Searchable__c, First_Name__c, Middle_Name__c, Last_Name__c, (Select SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c From Skills__r order by SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c), (Select Degree_Recieved__c, Field_Of_Study__c, School__c From Educations__r order by Graduation_Date__c), (Select CertificationName__r.Name__c From Certifications__r order by CertificationName__r.Name__c) FROM Resume__c Where Searchable__c = true Order By Last_Name__c]));
            } else if(filter=='First Name'){//Search for when a keyword is entered by first name
                setpeople = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Identifier__c, Searchable__c, First_Name__c, Middle_Name__c, Last_Name__c, (Select SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c From Skills__r order by SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c), (Select Degree_Recieved__c, Field_Of_Study__c, School__c From Educations__r order by Graduation_Date__c), (Select CertificationName__r.Name__c From Certifications__r order by CertificationName__r.Name__c) FROM Resume__c WHERE Searchable__c = true AND (First_Name__c like :NewSearchKeyword OR Last_Name__c like :NewSearchKeyword OR Id in :SkillList OR Id in :EduList OR Id in :CertList) Order By First_Name__c]));
            } else if(filter=='Last Name'){//Search for when a keyword is entered by last name
                setpeople = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Identifier__c, Searchable__c, First_Name__c, Middle_Name__c, Last_Name__c, (Select SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c From Skills__r order by SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c), (Select Degree_Recieved__c, Field_Of_Study__c, School__c From Educations__r order by Graduation_Date__c), (Select CertificationName__r.Name__c From Certifications__r order by CertificationName__r.Name__c) FROM Resume__c WHERE Searchable__c = true AND (First_Name__c like :NewSearchKeyword OR Last_Name__c like :NewSearchKeyword OR Id in :SkillList OR Id in :EduList OR Id in :CertList) Order By Last_Name__c]));
            }
            return setpeople;
        }
        set;
    }
    
    
    
    public List<Resume__c> getpeople() {//retrieves our query and turns it into a List with a datatype of Resume__c
        return (List<Resume__c>) setpeople.getRecords();
    }
    
    public String getSearchKeyword(){//gets the current SearchKeyword
        return SearchKeyword;
    }
    
    public void setSearchKeyword(String keyword){//Sets/Updates the SearchKeyword
        SearchKeyword=keyword;
    }
    
    public String getfilter(){//gets the current SearchKeyword
        return filter;
    }
    
    public void setfilter(String temp){//Sets/Updates the SearchKeyword
        filter=temp;
    }
    
    public PageReference SearchButton(){//Executes when the Search button is pressed
        getSearchKeyword();//updates the SearchKeyword so that the search can be preformed
        return null;
    }
    
}