public with sharing class OthersController{
    String who= ApexPages.currentPage().getParameters().get('who');//querestring for who's resume your viewing
    //quere the general information of the user from the resume object
    private final Resume__c resume = [SELECT Name, Last_Updated__c, First_Name__c, Middle_Name__c, Last_Name__c, Primary_Phone__c, Primary_Email__c, Agency_Holding_Clearance__c, Clearance__c, Polygraph__c FROM Resume__c
                   WHERE Identifier__c = :who];
    //List of my skills
    public List<Skill_Name__c> skill {get;set;}
    //List of my Educations
    List<Education__c> Edu;
    //List of my Certifications
    List<Certification__c> Cert;
    //List of my Projects
    List<Project__c> project;
    //If the Project edit popup should be displayed
    public Boolean displayPopup {get;set;}
    //for the homepage -> Search page to hold the querestring 
    public String SearchString {get;set;}
    
    //quere the Classification that the current user has
    public ApexPages.StandardSetController setclassification{
        get {
            if(setclassification== null) {
                setclassification= new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT User__c, Billable_Employee__c, Human_Resources__c, Bussiness_Development__c, System_Administrator__c, Executive__c FROM Classification__c WHERE User__c = :UserInfo.getUserId()]));
            }
            return setclassification;
        }
        set;
    }
    
    //retrieves the Classification that the current user has
    public Classification__c getClassifications() {
        Classification__c MyClassifications = new Classification__c();
        for(Classification__c temp : (List<Classification__c>) setclassification.getRecords()){
            MyClassifications.Billable_Employee__c = temp.Billable_Employee__c;
            MyClassifications.Human_Resources__c = temp.Human_Resources__c;
            MyClassifications.Bussiness_Development__c = temp.Bussiness_Development__c;
            MyClassifications.System_Administrator__c = temp.System_Administrator__c;
            MyClassifications.Executive__c = temp.Executive__c ;
        }
        return MyClassifications;
    }
    
    
    //displays the Project edit popup
    public void showPopup(){
        displayPopup = true;
    }
    
    //hides the Project edit popup
    public void closePopup(){
        displayPopup = false;
    }
    
    //sets the initial values of user's Skills
    public OthersController() {
        Skill = getSkills();
    }

    //starts the works flows of the 90 day wait
    public void UpdateDate(){
    
        //Cancel 90 Day workflow
        Date myDate = Date.parse('01/1/2001');
        resume.Last_Updated__c = myDate;
        update resume;
        
        //Update to today and start 90 day workflow
        resume.Last_Updated__c = today();
        update resume;
    }
    
    //quere the skills of who
    public ApexPages.StandardSetController setSkill {
        get {
            if(setSkill == null) {
                setSkill = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Name, Delete__c, Skill_Level__c, Resume__c, SkillName__r.Name__c, SkillName__r.SkillCategoryName__r.Name__c FROM Skill_name__c WHERE Resume__r.Identifier__c = :who ORDER BY SkillName__r.Name__c]));
            }
            return setSkill;
        }
        set;
    }
    
    //quere the skills of who
    public List<Skill_name__c> getSkills() {
        Skill = [SELECT Name, Delete__c, Skill_Level__c, Resume__c, SkillName__r.Name__c, SkillName__r.SkillCategoryName__r.Name__c FROM Skill_name__c WHERE Resume__r.Identifier__c = :who ORDER BY SkillName__r.SkillCategoryName__r.Name__c, SkillName__r.Name__c ASC];
        return Skill;
    }
    
    //quere the Educations of who
    public ApexPages.StandardSetController setEdu {
        get {
            if(setEdu == null) {
                setEdu = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Delete__c, City__c, State__c, Degree_Recieved__c, Field_Of_Study__c, Graduation_Date__c, School__c FROM Education__c WHERE Resume__r.Identifier__c = :who ORDER BY Graduation_Date__c ASC]));
            }
            return setEdu;
        }
        set;
    }
    
    //quere the Certifications of who
    public ApexPages.StandardSetController setCert {
        get {
            if(setCert == null) {
                setCert = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Delete__c, CertificationName__c, Certification_Name__c, Certification_Id__c, Date_Received__c, CertificationName__r.Name__c FROM Certification__c cert WHERE Resume__r.Identifier__c = :who ORDER BY Date_Received__c ASC]));
            }
            return setCert;
        }
        set;
    }
    
    //quere the Projects of who
    public ApexPages.StandardSetController setProject {
        get {
            if(setProject == null) {
                setProject = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Delete__c, Company__c, Current_Position__c, Project_Name__c, Position_Title__c, Project_Start__c, End_Date__c, Description__c FROM Project__c WHERE Resume__r.Identifier__c = :who ORDER BY Project_Start__c DESC]));
            }
            return setProject;
        }
        set;
    }
    
    //retrieve all the types of degrees from the default picklist
    public List<SelectOption> getDegreeRecievedList(){
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = Education__c.Degree_Recieved__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry temp : ple)
            {
                    options.add(new SelectOption(temp.getLabel(), temp.getValue()));
            }
            return options;
    }
    
    
    public String SelectedNewSkillName {get;set;}//the value of the new skill select list
    public String SelectedNewSkillCategoryName {get;set;}//the value of the new skill Category select list
    
    //the select list values of skills by the category the belong to
    public List<SelectOption> SkillNames{
        get{
            List<SelectOption> options = new List<SelectOption>();
            if(SelectedNewSkillCategoryName != null){
                options.add(new SelectOption('', '---Choose A Skill---'));
                if(SelectedNewSkillCategoryName != 'Other'){
                    List<SkillName__c> names = new List<SkillName__c>();
                    names = [SELECT Name__c, Name, SkillCategoryName__r.Name__c FROM SkillName__c Order By Name__c];
                    for(SkillName__c temp : names)
                    {
                        if(SelectedNewSkillCategoryName == temp.SkillCategoryName__r.id)
                            options.add(new SelectOption(temp.id, temp.Name__c));
                    }
                    options.add(new SelectOption('Other', 'Other'));
                }else{
                    options.add(new SelectOption('Other', 'Other'));
                }
            }
            return options;
            
        }
        set;
    }
    
    //the value that holds the Skill Category and the skills that depends on that skill Category
    public List<Tuple> MySkillList{get; set;}

    //get the user's skills by skill category
    public List<Tuple> getMySkills(){
        if(MySkillList==Null){
            MySkillList = new List<Tuple>();
            for(SkillCategoryName__c temp : [SELECT Name__c FROM SkillCategoryName__c]){
                List<Skill_Name__c> count = [Select id from Skill_Name__c WHERE SkillName__r.SkillCategoryName__r.Name__c =:temp.Name__c AND Resume__c= :resume.id];
                if(count.size() > 0){
                    MySkillList.add(new Tuple(temp.Name__c, [SELECT SkillName__r.Name__c, Skill_Level__c FROM Skill_Name__c Where Resume__c= :resume.id AND SkillName__r.SkillCategoryName__r.Name__c=:temp.Name__c Order By SkillName__r.Name__c]));
                }
            }
        }
        return MySkillList;
    }
    
    //the list of skillname for when editing a skill ** currently not used and was in a previous version
    public List<SelectOption> getEditSkillNames(){
        
            List<SelectOption> options = new List<SelectOption>();
                    for(SkillName__c temp : [SELECT Name__c, Name, SkillCategoryName__r.Name__c FROM SkillName__c Order By Name__c])
                    {
                            options.add(new SelectOption(temp.id, temp.Name__c));
                    }
                    options.add(new SelectOption('Other', 'Other'));
            return options;
    }
    
    //the list of Skill Category Names for when editing a skill ** currently not used and was in a previous version
    public List<SelectOption> getEditSkillCategoryNames(){
        
            List<SelectOption> options = new List<SelectOption>();
            for(SkillCategoryName__c temp : [SELECT Name__c, Name FROM SkillCategoryName__c Order By Name__c])
            {
                    options.add(new SelectOption(temp.id, temp.Name__c));
            }
            options.add(new SelectOption('Other', 'Other'));
            return options;
        
    }
    
    //Get all the skill categoryies
    public List<SelectOption> getSkillCategoryNames(){
        
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('', '---Choose A Category---'));
            List<SkillCategoryName__c> names = new List<SkillCategoryName__c>();
            names = [SELECT Name__c, Name FROM SkillCategoryName__c Order By Name__c];
            for(SkillCategoryName__c temp : names)
            {
                    options.add(new SelectOption(temp.id, temp.Name__c));
            }
            options.add(new SelectOption('Other', 'Other'));
            return options;
        
    }
    
    //retrieve the List of all the default Certification Names
    public List<SelectOption> CertNames{
        get{
            List<SelectOption> options = new List<SelectOption>();
            List<Certification_Name__c> names = new List<Certification_Name__c>();
            names = [SELECT Name__c, Name FROM Certification_Name__c Order By Name__c];
            for(Certification_Name__c temp : names)
            {
                    options.add(new SelectOption(temp.id, temp.Name__c));
            }
            options.add(new SelectOption('Other', 'Other'));
            return options;
        }
    }
    
    //SkillCategoryName Object for when the user selects other for the skillCategoryName
    public SkillCategoryName__c NewOtherSkillCategoryName{
        get{
            if (NewOtherSkillCategoryName== null)
                NewOtherSkillCategoryName= new SkillCategoryName__c();
            return NewOtherSkillCategoryName;
        }
        set;
    }

    //SkillName object for when the user selects other for the skillname
    public SkillName__c NewOtherSkillName{
        get{
            if (NewOtherSkillName== null)
                NewOtherSkillName= new SkillName__c();
            return NewOtherSkillName;
        }
        set;
    }
    
    //New SkillName object for adding a new Skill
    public SkillName__c NewSkillName{
        get {
          if (NewSkillName== null)
            NewSkillName = new SkillName__c();
          return NewSkillName;
        }
        set;
    }
    
    //New SkillCategoryName object for adding a new Skill
    public SkillCategoryName__c NewSkillCategoryName{
        get {
          if (NewSkillCategoryName== null)
            NewSkillCategoryName= new SkillCategoryName__c();
          return NewSkillCategoryName;
        }
        set;
    }
    
    //New Skill object for editing a skill ** currently not used anymore
    public Skill_Name__c NewEditSkill{
        get {
          if (NewSkill == null)
            NewSkill = new Skill_Name__c();
            NewSkill.Resume__c = resume.id;
          return NewSkill;
        }
        set;
    }
    
    //New skill object for adding a new skill
    public Skill_Name__c NewSkill{
        get {
          if (NewSkill == null)
            NewSkill = new Skill_Name__c();
            NewSkill.Resume__c = resume.id;
          return NewSkill;
        }
        set;
    }
    
    //New Education object for adding a new Education
    public Education__c NewEdu{
        get {
          if (NewEdu == null)
            NewEdu = new Education__c ();
            NewEdu.Resume__c = resume.id;
          return NewEdu;
        }
        set;
    }
    
    //New Certification object for adding a new Certification
    public Certification__c NewCert{
        get {
          if (NewCert == null)
            NewCert= new Certification__c ();
            NewCert.Resume__c = resume.id;
          return NewCert;
        }
        set;
    }
    
    //New Certification Name object for adding a new Certification
    public Certification_Name__c NewCertName{
        get {
          if (NewCertName == null)
            NewCertName = new Certification_Name__c ();
          return NewCertName;
        }
        set;
    }
    
    //New Certification object for adding a new Certification and the user select other for the Certification Name
    public Certification_Name__c NewOtherCertName{
        get {
          if (NewOtherCertName == null)
            NewOtherCertName = new Certification_Name__c();
          return NewOtherCertName;
        }
        set;
    }
    
    //New Project object for adding a new Project
    public Project__c NewProject{
        get {
          if (NewProject== null)
            NewProject= new Project__c ();
            NewProject.Resume__c = resume.id;
          return NewProject;
        }
        set;
    }
    
    //retrieve the User's List of Educations
    public List<Education__c> getEdu() {
        return (List<Education__c>) setEdu.getRecords();
    }

    //retrieve the User's List of Certifications
    public List<Certification__c> getCert() {
        return (List<Certification__c>) setCert.getRecords();
    }
    
    //retrieve the User's List of Projects
    public List<Project__c> getProject() {
        return (List<Project__c>) setProject.getRecords();
    }
    

    //retrieve the User's General Information
    public Resume__c getResume() {
        return resume;
    }

    //Returns today's date
    public Date today(){
        Date d;
        d = Date.today();
        return d;
    }
    
    //creates an audit in the audit table
    public void audit(String NewValue, String table){
        Audits__c audit = new audits__c();
        audit.NewValue__c = NewValue;
        audit.DateChanged__c = today();
        audit.Editor__c = UserInfo.getUserId();
        audit.UserEdited__c = who;
        audit.Archive__c = false;
        audit.ActionTaken__c = 'No Action Taken';
        audit.TableEdited__c = table;
        insert audit;
    }
    
    //adds a new skill to the user's resume
    public PageReference saveSkill() {
    
        if(SelectedNewSkillCategoryName == 'Other'){//if Skill Category selected is other, then use both of the text fields that appears
            NewSkillCategoryName.Name__c = NewOtherSkillCategoryName.Name__c;
            insert NewSkillCategoryName;
            
            NewSkillName.Name__c = NewOtherSkillName.Name__c;
            SkillCategoryName__c tempCat = [Select id from SkillCategoryName__c Where Name__c = :NewOtherSkillCategoryName.Name__c];
            NewSkillName.SkillCategoryName__c = tempCat.id;
            insert NewSkillName;
            
            SkillName__c tempSkill = [Select id, Name__c from SkillName__c Where Name__c = :NewOtherSkillName.Name__c AND SkillCategoryName__c =: tempCat.id];
            NewSkill.SkillName__c = tempSkill.id;
            audit(tempSkill.Name__c, 'Skill_Name__c');
            insert NewSkill;
            
        } else if(SelectedNewSkillName == 'Other'){//if Skill Name selected is other, then use the text field that appears
            NewSkillName.Name__c = NewOtherSkillName.Name__c;
            NewSkillName.SkillCategoryName__c = SelectedNewSkillCategoryName;
            insert NewSkillName;
            
            SkillName__c tempSkill = [Select id, Name__c from SkillName__c Where Name__c = :NewOtherSkillName.Name__c AND SkillCategoryName__c =: SelectedNewSkillCategoryName];
            NewSkill.SkillName__c = tempSkill.id;
            audit(tempSkill.Name__c, 'Skill_Name__c');
            insert NewSkill;
        }else{//if only the default values are selected
            NewSkill.SkillName__c = SelectedNewSkillName;
            insert NewSkill;
        }
        UpdateDate();
            
        //refresh the page
        pagereference mypage= new PageReference('/apex/OthersResume?ctab=Skills&who='+who);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //add a new Education to the User's Resume
    public PageReference saveEdu() {
        insert NewEdu;
        UpdateDate();
        pagereference redirect = new PageReference('/apex/OthersResume?ctab=Education&who='+who);
        redirect.setRedirect(true);
        system.debug('Hello@@'+redirect);
        return redirect;
    }
    
    //the Certification Name selected from the SelectList
    public String SelectedCertName {get;set;}
    
    //add a new Certification to the User's Resume
    public PageReference saveCert() {//if the Certification Name is other, then use the textfield that appears
        if(SelectedCertName =='Other'){
            NewCertName.Name__c = NewOtherCertName.Name__c;
            insert NewCertName;
            
            Certification_Name__c tempCert = [Select id, Name__c from Certification_Name__c Where Name__c = :NewOtherCertName.Name__c];
            NewCert.CertificationName__c = tempCert.id;
            //audit(tempCert.Name__c, 'Certification__c');
            insert NewCert;
        }else{//only using the default values from the SelectList
        
            NewCert.CertificationName__c = SelectedCertName;
            insert NewCert;
        }
        UpdateDate();
        pagereference redirect = new PageReference('/apex/OthersResume?ctab=Education&who='+who);
        redirect.setRedirect(true);
        system.debug('Hello@@'+redirect);
        return redirect;
    }
    
    //add a new Project to the User's Resume
    public PageReference saveProject() {
        insert NewProject;
        UpdateDate();
        pagereference redirect = new PageReference('/apex/OthersResume?ctab=Projects&who='+who);
        redirect.setRedirect(true);
        system.debug('Hello@@'+redirect);
        return redirect;
    }
    
    //Edit the User's General Information
    public PageReference EditGeneralInfo() {
        displayPopup = false;
        update resume;
        UpdateDate();
        pagereference mypage= new PageReference('/apex/OthersResume?ctab=Skills&who='+who);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //Edit the User's Projects section
    public PageReference EditProject() {
        displayPopup = false;
        update getProject();
        UpdateDate();
        pagereference mypage= new PageReference('/apex/OthersResume?ctab=Projects&who='+who);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //Delete all of the Checked skills
    public PageReference DeleteSkill() {
        update Skill;//save all the check boxes marked as true
        UpdateDate();
        List<Skill_Name__c> deleteList = [SELECT id FROM Skill_Name__c WHERE Delete__c = True];
        for (Skill_Name__c DeleteSkill : deleteList){
            delete DeleteSkill;//delete the records that are checked
        }
        pagereference mypage= new PageReference('/apex/OthersResume?ctab=Skills&who='+who);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //Delete all the check Educations
    public PageReference DeleteEducation() {
        update getEdu();
        UpdateDate();
        List<Education__c> deleteList = [SELECT id FROM Education__c WHERE Delete__c = True];
        for (Education__c DeleteEdu : deleteList){
            delete DeleteEdu;
        }
        pagereference mypage= new PageReference('/apex/OthersResume?ctab=Education&who='+who);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //Delete all the check Certifications
    public PageReference DeleteCertification() {
        update getCert();
        UpdateDate();
        List<Certification__c> deleteList = [SELECT id FROM Certification__c WHERE Delete__c = True];
        for (Certification__c DeleteCert : deleteList){
            delete DeleteCert;
        }
        pagereference mypage= new PageReference('/apex/OthersResume?ctab=Education&who='+who);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //Delete all the check Projects
    public PageReference DeleteProject() {
        update getProject();
        UpdateDate();
        List<Project__c> deleteList = [SELECT id FROM Project__c WHERE Delete__c = True];
        for (Project__c DeleteProject : deleteList){
            delete DeleteProject;
        }
        pagereference mypage= new PageReference('/apex/OthersResume?ctab=Projects&who='+who);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //View the User's corporate resume
    public PageReference ViewResume() {
        pagereference mypage= new PageReference('/apex/OthersCorporateResume?who='+who);
        mypage.setRedirect(true);
        return mypage;
    }
    
    //a wrapper used to map the skill Category to the List of skills that are dependent of that category.
    public class Tuple{
        public String value1 {get;set;}
        public List<Skill_Name__c> value2 {get;set;}
        
        public Tuple(String val1, List<Skill_Name__c> val2){
            value1 = val1;
            value2 = val2;
        }
    }
}