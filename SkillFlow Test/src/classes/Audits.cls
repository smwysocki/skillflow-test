public with sharing class Audits{
    public boolean showall = false;//toggle show history
    public Audits(){
        
    }
    
    //return the current value of showall for form
    public boolean getShow(){
            return showall;
    }
    
    //quere the Classifications for a user
    public ApexPages.StandardSetController setclassification{
        get {
            if(setclassification== null) {
                setclassification= new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT User__c, Billable_Employee__c, Human_Resources__c, Bussiness_Development__c, System_Administrator__c, Executive__c FROM Classification__c WHERE User__c = :UserInfo.getUserId()]));
            }
            return setclassification;
        }
        set;
    }
    
    //retrieves the classification for a user.
    public Classification__c getClassifications() {
        Classification__c MyClassifications = new Classification__c();
        for(Classification__c temp : (List<Classification__c>) setclassification.getRecords()){
            MyClassifications.Billable_Employee__c = temp.Billable_Employee__c;
            MyClassifications.Human_Resources__c = temp.Human_Resources__c;
            MyClassifications.Bussiness_Development__c = temp.Bussiness_Development__c;
            MyClassifications.System_Administrator__c = temp.System_Administrator__c;
            MyClassifications.Executive__c = temp.Executive__c ;
        }
        return MyClassifications;
    }
    
    //quere the audit table for those that have not had any actioin yet
    public ApexPages.StandardSetController setaudit{
        get {
            if(setaudit == null) {
                setaudit = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT id, reason__c, select__c, TableEdited__c, NewValue__c, DateChanged__c, UserEdited__c, Editor__c, FieldID__c, Archive__c, ActionTaken__c FROM Audits__c Where Archive__c = false ORDER BY DateChanged__c DESC]));
            }
            return setaudit;
        }
        set;
    }
    
    //quere all rows in the audit table
    public ApexPages.StandardSetController setHistory{
        get {
            if(setHistory== null) {
                setHistory= new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT id, reason__c, select__c, TableEdited__c, NewValue__c, DateChanged__c, UserEdited__c, Editor__c, FieldID__c, Archive__c, ActionTaken__c FROM Audits__c ORDER BY Archive__c, DateChanged__c DESC]));
            }
            return setHistory;
        }
        set;
    }
    
    //retrieves the audit table of those with no action taken
    public List<Audits__c> getAudits() {
        return (List<Audits__c>) setaudit.getRecords();
    }
    
    //retrieves all rows in the audit table
    public List<Audits__c> getHistory() {
        return (List<Audits__c>) setHistory.getRecords();
    }
    
    //retrieved the audit table by the current page ** Currently inactive
    /*public List<Audits__c> getAudit(){
        return getAudits(page);
    }*/
    
    //queried the audit table for those with no action taken by page ** Currently not used
    public List<Audits__c> getAudits(Integer pages) {
        Integer Start = (page - 1) * 25;
        Integer count = 0;
        Integer currentRecord = 0;
        
        List<Audits__c> tempAudits = new List<Audits__c>();
        List<Audits__c> AuditList = [SELECT reason__c, select__c, TableEdited__c, OldValue__c, NewValue__c, DateChanged__c, UserEdited__c, Editor__c, FieldID__c, Archive__c, ActionTaken__c FROM Audits__c Where Archive__c = false ORDER BY DateChanged__c DESC];
        TotalAuditRecords = AuditList.size();
        for(Audits__c temp : AuditList){
            If(CurrentRecord >= start){
                tempAudits.add(new Audits__c(reason__c = temp.reason__c, select__c = temp.select__c, TableEdited__c = temp.TableEdited__c, NewValue__c = temp.NewValue__c, DateChanged__c = temp.DateChanged__c, UserEdited__c = temp.UserEdited__c, Editor__c = temp.Editor__c, FieldID__c = temp.FieldID__c, Archive__c = temp.Archive__c, ActionTaken__c = temp.ActionTaken__c));
                count++;
            }
            If(count >= 25)
                Break;
            currentRecord++;
        }
        return tempAudits;
    }
    
    //retrieved all rows of the audit table by the current page ** Currently inactive
    /*public List<Audits__c> getHistory(){
        return getHistorys(page);
    }*/
    //queried all rows of the audit table by page ** Currently not used
    public List<Audits__c> getHistorys(Integer pages) {
        Integer Start = (page - 1) * 25;
        Integer count = 0;
        Integer currentRecord = 0;
        
        List<Audits__c> tempAudits = new List<Audits__c>();
        List<Audits__c> AuditList = [SELECT reason__c, select__c, TableEdited__c, OldValue__c, NewValue__c, DateChanged__c, UserEdited__c, Editor__c, FieldID__c, Archive__c, ActionTaken__c FROM Audits__c ORDER BY Archive__c, DateChanged__c DESC];
        TotalAuditRecords = AuditList.size();
        for(Audits__c temp : AuditList){
            If(CurrentRecord >= start){
                tempAudits.add(new Audits__c(reason__c = temp.reason__c, select__c = temp.select__c, TableEdited__c = temp.TableEdited__c, NewValue__c = temp.NewValue__c, DateChanged__c = temp.DateChanged__c, UserEdited__c = temp.UserEdited__c, Editor__c = temp.Editor__c, FieldID__c = temp.FieldID__c, Archive__c = temp.Archive__c, ActionTaken__c = temp.ActionTaken__c));
                count++;
            }
            If(count >= 25)
                Break;
            currentRecord++;
        }
        return tempAudits;
    }
    
    //return the current date
    public Date today(){
        Date d;
        d = Date.today();
        return d;
    }
    
    //approves an audit in the audit table
    public PageReference Approve() {
        
        //which table is currently used to be edited, the checkmark is updated to identify which records need to be updated. 
        if(!showall)
            update getAudits();
        else
            update getHistory();
            
        //approves the marked rows
        List<audits__c> audits = [Select Archive__c, ActionTaken__c, DateChanged__c, Select__c From Audits__c where Select__c = true];
        for(Audits__c audit : audits){
            audit.Select__c = False; //sets it back to false so that other rows can be approved or rejected in the future
            audit.ActionTaken__c = 'Approved';
            audit.DateChanged__c = today();
            audit.Archive__c = true;
        }
        update audits; //updates the changed fields
        
        //refreshes the page
        pagereference mypage= new PageReference('/apex/All_Audits');
        mypage.setRedirect(true);
        return mypage;
    }
    
    public PageReference Reject() {
        
        //which List are we using and mark the rows we are rejecting
        if(!showall)
            update getAudits();
        else
            update getHistory();
            
        //reject the marked rows
        List<audits__c> audits = [Select TableEdited__c, NewValue__c, Archive__c, ActionTaken__c, DateChanged__c, Select__c From Audits__c where Select__c = true];
        for(Audits__c audit : audits){
            audit.Select__c = False;
            audit.ActionTaken__c = 'Rejected';
            audit.DateChanged__c = today();
            audit.Archive__c = true;
            if(audit.TableEdited__c == 'Skill_Name__c'){//if the row is a skill we are modifying
                for(Skill_Name__c skills : [Select id from Skill_Name__c where SkillName__r.Name__c =: audit.NewValue__c]){
                    delete skills;//delete the skill from all users that have this skill.
                }
                for(SkillName__c skillNames : [Select id, Name__c from SkillName__c where Name__c =: audit.NewValue__c]){
                    delete skillNames;//delete this skill from the skill dropdown
                }
                Boolean CategoryFlag = false;
                List<SkillCategoryName__c> SkillCategoryDeleteList = new List<SkillCategoryName__c>();
                for(SkillCategoryName__c allCategories : [Select id from SkillCategoryName__c]){
                    for(AggregateResult findCatIDs : [Select SkillCategoryName__r.id CatID, SkillCategoryName__r.Name__c from SkillName__c group by SkillCategoryName__r.id, SkillCategoryName__r.Name__c Having count(id) > 0]){
                        if(allCategories.id == (String) findCatIDs.get('CatID')){
                            CategoryFlag = true;
                        }
                    }
                    if(!CategoryFlag){
                        SkillCategoryDeleteList.add(allCategories);
                        
                    }
                    CategoryFlag = false;
                }
                
                for(SkillCategoryName__c DeleteSkillCategory : SkillCategoryDeleteList){
                    delete DeleteSkillCategory;
                }
            } else if(audit.TableEdited__c == 'Certification__c'){//if this row is a certification
                for(Certification__c Certification : [Select id from Certification__c where CertificationName__r.Name__c =: audit.NewValue__c]){
                    delete Certification;// delete all certification records that have this certification
                }
                for(Certification_Name__c CertificationName : [Select id, Name__c from Certification_Name__c where Name__c =: audit.NewValue__c]){
                    delete CertificationName;//delete the certification from the dropdown
                }
            }
        }
        update audits;//update the audit
        
        //refresh the page
        pagereference mypage= new PageReference('/apex/All_Audits');
        mypage.setRedirect(true);
        return mypage;
    }
    
    //toggle the History of audits
    public PageReference ShowAll() {
        if(showall)
            showall = false;
        else
            showall = true;
        return null;
    }
    
    //a counter for what page the audit table is displaying ** Currently not used
    Public Integer page{
        get{
            if(page == null)
                page = 1;
            return page;
        }
        set{
            page = 1;
        }
    
    }
    
    //number of total records in the audit table currently ** Currently not used
    Public Integer TotalAuditRecords {get;set;}
    
    //buttons to change from page to page in the audit table ** Currently inactive
    /*public PageReference PreviousPage() {
        if((page+1)*25 <= TotalAuditRecords){
            page++;
            getAudits(page);
        }
        return null;
    }
    
    public PageReference NextPage() {
        if((page-1) > 0){
            page--;
            getAudits(page);
        }
        return null;
    }*/
    
}