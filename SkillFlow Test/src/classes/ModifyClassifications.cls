Public class ModifyClassifications{

    Public ModifyClassifications(){
        
    }
    
    //total number of users or rows in the classification table below
    Public Integer TotalUsers {get;set;}
    
    
    //quere the Classifications for a user
    public ApexPages.StandardSetController setclassification{
        get {
            if(setclassification== null) {
                setclassification= new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT User__c, Billable_Employee__c, Human_Resources__c, Bussiness_Development__c, System_Administrator__c, Executive__c FROM Classification__c WHERE User__c = :UserInfo.getUserId()]));
            }
            return setclassification;
        }
        set;
    }
    
    //retrieves the classification for a user.
    public Classification__c getClassifications() {
        Classification__c MyClassifications = new Classification__c();
        for(Classification__c temp : (List<Classification__c>) setclassification.getRecords()){
            MyClassifications.Billable_Employee__c = temp.Billable_Employee__c;
            MyClassifications.Human_Resources__c = temp.Human_Resources__c;
            MyClassifications.Bussiness_Development__c = temp.Bussiness_Development__c;
            MyClassifications.System_Administrator__c = temp.System_Administrator__c;
            MyClassifications.Executive__c = temp.Executive__c ;
        }
        return MyClassifications;
    }
    
    //Current page ** currently not used
    public Integer Page{
        get{
            if(Page==null){
                Page = 1;
            }
            return Page;
        }
        set;
    }
    
    //retrieves the Classification table by page ** currently not used
    /*public List<Classification> getClassList(){
        return getClassLists(page);
    }*/
    
    //wraps the user's classification with their first and last name
    //this was done because I couldnt quere from both the User table and the Classification table in 1 quere
    public Class Classification{
        public String FirstName {get;set;}
        public String LastName {get;set;}
        public Classification__c MyCurrentclassification {get;set;}
        
        public Classification(String First, String Last, Classification__c Myclassification){
            FirstName = First;
            LastName = Last;
            MyCurrentclassification = Myclassification;
        }
    }
    
    //retrieves the Classification table 
    public List<Classification> ClassList{
        get{
            if(ClassList==null){
                TotalUsers = ClassificationList.size();
                ClassList = new List<Classification>();
                for(Classification__c temp : ClassificationList){
                        String FirstName = '';
                        for (User tempFName : [Select FirstName From User WHERE id=:temp.User__c LIMIT 1]){
                            FirstName = tempFName.FirstName;
                        }
                        
                        String LastName = '';
                        for (User tempLName : [Select LastName From User WHERE id=:temp.User__c LIMIT 1]){
                            LastName = tempLName.LastName;
                        }
                        
                        ClassList.add(new Classification(FirstName, LastName, (new Classification__c(id = temp.id, User__c = temp.User__c, System_Administrator__c = temp.System_Administrator__c, Human_Resources__c = temp.Human_Resources__c, Executive__c = temp.Executive__c, Bussiness_Development__c = temp.Bussiness_Development__c, Billable_Employee__c = temp.Billable_Employee__c))));
                }
            }
            return ClassList;
        }
        set;
    }
    
    //retrieves the classification table by page ** currently not used
    /*public List<Classification> getClassLists(Integer pages){
        Integer Start = (pages - 1) * 25;
        Integer count = 0;
        Integer currentRecord = 0;
        
        TotalUsers = ClassificationList.size();
        List<Classification> tempClass = new List<Classification>();
        for(Classification__c temp : ClassificationList){
            
            
            If(CurrentRecord >= start){
                
                String FirstName = '';
                for (User tempFName : [Select FirstName From User WHERE id=:temp.User__c LIMIT 1]){
                    FirstName = tempFName.FirstName;
                }
                
                String LastName = '';
                for (User tempLName : [Select LastName From User WHERE id=:temp.User__c LIMIT 1]){
                    LastName = tempLName.LastName;
                }
                
                tempClass.add(new Classification(FirstName, LastName, (new Classification__c(id = temp.id, User__c = temp.User__c, System_Administrator__c = temp.System_Administrator__c, Human_Resources__c = temp.Human_Resources__c, Executive__c = temp.Executive__c, Bussiness_Development__c = temp.Bussiness_Development__c, Billable_Employee__c = temp.Billable_Employee__c))));
                count++;
            }
            If(count >= 25)
                Break;
            currentRecord++;
        }
        return tempClass;
    }*/    
    
    //queries the classification of each user
    public List<Classification__c> ClassificationList{
        get{
            if(ClassificationList == null){
                ClassificationList = [Select id, User__c, System_Administrator__c, Human_Resources__c, Executive__c, Bussiness_Development__c, Billable_Employee__c From Classification__c order by Resume__r.Last_Name__c];
            }
            return ClassificationList;
        }
        set;
    }
    
    //actions used to page forward and back for the Classification table
    /*public PageReference PreviousPage() {
        if((page+1)*25 <= TotalUsers){
            page++;
            getClassLists(page);
        }
        return null;
    }*/
    
    /*public PageReference NextPage() {
        if((page-1) > 0){
            page--;
            getClassLists(page);
        }
        return null;
    }*/
    
    //save the modification of all the classifications
    public PageReference Save() {
        for (Classification UpdatedClassification : ClassList){
            update UpdatedClassification.MyCurrentclassification;
        }
        
        List<Classification__c> ClassList = [Select User__c, Billable_Employee__c from Classification__c];
        for(Classification__c tempClass : ClassList){
            For(Resume__c tempResume : [Select Searchable__c From Resume__c Where Identifier__c =: tempClass.User__c]){
                tempResume.Searchable__c = tempClass.Billable_Employee__c;
                update tempResume;
            }
        }
        return null;
    }
}