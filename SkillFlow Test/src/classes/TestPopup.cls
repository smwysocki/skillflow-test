public with sharing class TestPopup{

    //all the controllers that have access to this class
    public TestPopup(MyController controller) {

    }
    public TestPopup(OthersController controller) {

    }
    public TestPopup(Audits controller) {

    }
    
    //weither to show or hide the popup
    public Boolean displayPopup {get;set;}

    
    //show the popup
    public void showPopup(){
        displayPopup = true;
    }
    
    //hide the popup
    public void closePopup(){
        displayPopup = false;
    }
}