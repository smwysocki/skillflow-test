Public Class NewUser{
    public NewUser(){
    
    }
    
    //quere the Classifications for a user
    public ApexPages.StandardSetController setclassification{
        get {
            if(setclassification== null) {
                setclassification= new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT User__c, Billable_Employee__c, Human_Resources__c, Bussiness_Development__c, System_Administrator__c, Executive__c FROM Classification__c WHERE User__c = :UserInfo.getUserId()]));
            }
            return setclassification;
        }
        set;
    }
    
    //retrieves the classification for a user.
    public Classification__c getClassifications() {
        Classification__c MyClassifications = new Classification__c();
        for(Classification__c temp : (List<Classification__c>) setclassification.getRecords()){
            MyClassifications.Billable_Employee__c = temp.Billable_Employee__c;
            MyClassifications.Human_Resources__c = temp.Human_Resources__c;
            MyClassifications.Bussiness_Development__c = temp.Bussiness_Development__c;
            MyClassifications.System_Administrator__c = temp.System_Administrator__c;
            MyClassifications.Executive__c = temp.Executive__c ;
        }
        return MyClassifications;
    }
    
    //Create a new classification record for the new user
    public Classification__c NewClassification{
        get{
            if(NewClassification == null){
                NewClassification = new Classification__c();
            }
            return NewClassification;
        }
        set;
    }
    
    //Create a new Resume record for the new user
    public Resume__c NewResume{
        get{
            if(NewResume == null){
                NewResume = new Resume__c();
                for(User myInfo : [Select Firstname, lastname, email from User where id=:NewClassification.User__c]){
                    NewResume.First_Name__c = myInfo.Firstname;
                    NewResume.Last_Name__c = myInfo.Lastname;
                    NewResume.Primary_Email__c = myInfo.email;
                }
            }
            return NewResume;
        }
        set;
    }
    
    public User currentUser {get;set;}
    
    public Boolean UserExists{
        get{
            return UserExists;
        }
        set{
            UserExists=true;
        }
    }
    
    //action to save the new classification and resume
    public PageReference CreateUser() {
        List<Resume__c> CheckUser = [Select id from Resume__c where Identifier__c=: NewClassification.User__c];
        if(NewClassification.User__c == null){
            return null;
        } else if(!(CheckUser.size() > 0)){
            if(NewClassification.Billable_Employee__c){//only billable employees should be searchable
                NewResume.Identifier__c = NewClassification.User__c;
                NewResume.Searchable__c = true;
                Insert NewResume;
                List<Resume__c> tempResume = [Select id from Resume__c where Primary_Email__c =: NewResume.Primary_Email__c];
                for(Resume__c temp : tempResume){
                    NewClassification.Resume__c = temp.id;
                }
            } else {
                NewResume.Identifier__c = NewClassification.User__c;
                NewResume.Searchable__c = false;
                for(User tempUser : [Select FirstName, LastName, Email From User Where id =: NewClassification.User__c]){
                    NewResume.First_Name__c = tempUser.FirstName;
                    NewResume.Last_Name__c = tempUser.LastName;
                    NewResume.Primary_Email__c = tempUser.Email;
                    NewResume.Primary_Phone__c = tempUser.Phone;
                }
                Insert NewResume;//everyone gets a resume record since they could potenchally be switch to billable employee status
                List<Resume__c> tempResume = [Select id from Resume__c where Primary_Email__c =: NewResume.Primary_Email__c];
                for(Resume__c temp : tempResume){
                    NewClassification.Resume__c = temp.id;
                }
            }
            Insert NewClassification;
            //refresh page
            pagereference mypage= new PageReference('/apex/New_User');
            mypage.setRedirect(true);
            return mypage;
        } else {
            //User Already Exists
            UserExists = false;
            for(User tempUser : [Select FirstName, LastName from User where id =: NewClassification.User__c]){
                CurrentUser = tempUser;
            }
            return null;
        }
    }
    
    public PageReference repeatUserOkBTN() {
        pagereference mypage= new PageReference('/apex/New_User');
        mypage.setRedirect(true);
        return mypage;
    }
}