public class GapAnalysis{
    String ViewLOB = null;
    private final Line_Of_Business__c LOB;
    List<Line_Of_Business__c> Business = new List<Line_Of_Business__c>();
    
    public ApexPages.StandardSetController setclassification{
        get {
            if(setclassification== null) {
                setclassification= new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT User__c, Billable_Employee__c, Human_Resources__c, Bussiness_Development__c, System_Administrator__c, Executive__c FROM Classification__c WHERE User__c = :UserInfo.getUserId()]));
            }
            return setclassification;
        }
        set;
    }
    
    public Classification__c getClassifications() {
        Classification__c MyClassifications = new Classification__c();
        for(Classification__c temp : (List<Classification__c>) setclassification.getRecords()){
            MyClassifications.Billable_Employee__c = temp.Billable_Employee__c;
            MyClassifications.Human_Resources__c = temp.Human_Resources__c;
            MyClassifications.Bussiness_Development__c = temp.Bussiness_Development__c;
            MyClassifications.System_Administrator__c = temp.System_Administrator__c;
            MyClassifications.Executive__c = temp.Executive__c ;
        }
        return MyClassifications;
    }
    
    public String NewLine {
        get{
            String NewLine = Apexpages.currentPage().getParameters().get('NewLine');
        return NewLine;
        }
    }  
    
    public ApexPages.StandardSetController setSkills {
        get {
            if(setSkills == null) {
            String NewLine = Apexpages.currentPage().getParameters().get('NewLine');
                setSkills = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Delete__c, Skills__r.Name__c, Skills__r.SkillCategoryName__r.Name__c FROM LOBSkill__c WHERE Line_Of_Business__r.Name__c = :NewLine]));
            }
            return setSkills;
        }
        set;
    }    
    
    public List<LOBSkill__c> getSkills() {
        return (List<LOBSkill__c>) setSkills.getRecords();
    }
    
// Class to display the skills drop down on the Gap Analysis page
       
       public String SelectedNewSkillName {get;set;}
    public String SelectedNewSkillCategoryName {get;set;}
    
    public List<SelectOption> getSkillCategoryNames(){
        
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('', '---Choose A Category---'));
            List<SkillCategoryName__c> names = new List<SkillCategoryName__c>();
            names = [SELECT Name__c, Name FROM SkillCategoryName__c Order By Name__c];
            for(SkillCategoryName__c temp : names)
            {
                    options.add(new SelectOption(temp.id, temp.Name__c));
            }
            return options;
        
    }
    
    public List<SelectOption> SkillNames{
        get{
            List<SelectOption> options = new List<SelectOption>();
            if(SelectedNewSkillCategoryName != null){
                options.add(new SelectOption('', '---Choose A Skill---'));
                if(SelectedNewSkillCategoryName != 'Other'){
                    List<SkillName__c> names = new List<SkillName__c>();
                    names = [SELECT Name__c, Name, SkillCategoryName__r.Name__c FROM SkillName__c Order By Name__c];
                    for(SkillName__c temp : names)
                    {
                        if(SelectedNewSkillCategoryName == temp.SkillCategoryName__r.id)
                            options.add(new SelectOption(temp.id, temp.Name__c));
                    }
                }
            }
            return options;
            
        }
        set;
    }
    
    public List<Tuple> MySkillList{get; set;}

    
       
       public class Tuple{
        public String value1 {get;set;}
        public List<Skill_Name__c> value2 {get;set;}
        
        public Tuple(String val1, List<Skill_Name__c> val2){
            value1 = val1;
            value2 = val2;
        }
    }
       
     List<Skill_Name__c> skills;
       public Skill_Name__c NewSkill{
        get {
          if (NewSkill == null)
            NewSkill = new Skill_Name__c();
          return NewSkill;
        }
        set;
    }
        
    public Line_Of_Business__c NewLOB{
    get {
      if (NewLOB== null)
        NewLOB= new Line_Of_Business__c();
      return NewLOB;
    }
    set;
  }

// Class to display the already created lines of business 

    public List<SelectOption> getBusiness()
    {
        List<SelectOption> options = new List<SelectOption>();
        Business= [SELECT Name__c from Line_Of_Business__c limit 100];
        if (Business.Size() >0)
        {
            
            for(Line_Of_Business__c c:Business)
            {
                if (c.Name__c != null)
                options.add(new SelectOption(c.Name__c,c.Name__c));
            }
        }
        return options;
    }  

   public String getViewLOB(){
      return ViewLOB;
    }
  
    public void setViewLOB(String ViewLOB) {
        this.ViewLOB= ViewLOB;
    }

  
    
    public LOBSkill__c NewLOBSkill{
    get {
      if (NewLOBSkill == null)
        NewLOBSkill = new LOBSkill__c();
      return NewLOBSkill;
    }
    set;
  }

    public Line_Of_Business__c getLOB() {
        return LOB;
    }


    public List<Data> getData() {
        return GapAnalysis.getChartData();
    }
    
    @RemoteAction
    public static List<Data> getRemoteData() {
        return GapAnalysis.getChartData();
    }

    // The actual chart data; needs to be static to be
    // called by a @RemoteAction method
    public static List<Data> getChartData() {
        String NewLine = Apexpages.currentPage().getParameters().get('NewLine');
        List<Data> data = new List<Data>();
        List<LOBSkill__c> tempLOB = [Select Skills__r.Name__c From LOBSkill__c Where Line_Of_Business__r.Name__c=:NewLine];
        for(LOBSkill__c LOBLoop : tempLOB){
            boolean tempflag = false;
            for(AggregateResult temp : [select SkillName__r.Name__c a, count(id) from Skill_Name__c where resume__r.searchable__c = true group by SkillName__r.Name__c order by SkillName__r.Name__c]){
                if((String)temp.Get('a') == LOBLoop.Skills__r.Name__c){
                    data.add(new Data((String)temp.Get('a'), (Integer) temp.Get('expr0')));
                    tempflag = true;
                }
            }
            if(!tempflag){
                data.add(new Data(LOBLoop.Skills__r.Name__c, 0));
            }
        }
        return data;
    }
    
    // Wrapper class
    public class Data {
        public String name { get; set; }
        public Integer people { get; set; }
        public Data(String name, Integer people) {
            this.name = name;
            this.people = people;
        }
    }
    
    public List<Pie1> getPie1() {
        return GapAnalysis.getChartPie1();
    }
    
    @RemoteAction
    public static List<Pie1> getRemotePie1() {
        return GapAnalysis.getChartPie1();
    }

    // The actual chart data; needs to be static to be
    // called by a @RemoteAction method
    public static List<Pie1> getChartPie1() {
        String NewLine = Apexpages.currentPage().getParameters().get('NewLine');
        List<Pie1> Pie1 = new List<Pie1>();
        List<AggregateResult> q = [Select Resume__r.id, count(SkillName__r.id) from Skill_Name__c where SkillName__c in (Select Skills__c From LOBSkill__c Where Line_Of_Business__r.Name__c=:NewLine) and resume__r.searchable__c = true group by Resume__r.id];
        Integer Max = 0;
        integer total = database.countQuery('select count() from Skill_Name__c');
        for(AggregateResult temp : q){
            if((Integer) temp.Get('expr0') > Max)
                Max = (Integer) temp.Get('expr0');
        }
        Integer x = Max;
        While(x >= 0){
            Integer Count = 0;
            for(AggregateResult temp : q){
                if((Integer) temp.Get('expr0') == x)
                    count++;
            }
            If(count>0){
                Double percent = ((Double) count);
                Pie1.add(new Pie1((count + ' Billable Employees have ' + x + ' skills.'), percent));
            }
            x--;
        }
        return Pie1;
    }
    
    // Wrapper class
    public class Pie1{
        public String name { get; set; }
        public Double percent { get; set; }
        public Pie1(String name, Double percent) {
            this.name = name;
            this.percent = percent;
        }
    }
    
    
    public List<Pie2> getPie2() {
        return GapAnalysis.getChartPie2();
    }
    
    @RemoteAction
    public static List<Pie2> getRemotePie2() {
        return GapAnalysis.getChartPie2();
    }

    // The actual chart data; needs to be static to be
    // called by a @RemoteAction method
    public static List<Pie2> getChartPie2() {
        String NewLine = Apexpages.currentPage().getParameters().get('NewLine');
        List<Pie2> Pie2 = new List<Pie2>();
        integer total = database.countQuery('select count() from Skill_Name__c');
        for(AggregateResult temp : [select Skill_Level__c a, count(id) from Skill_Name__c where SkillName__c in (Select Skills__c From LOBSkill__c Where Line_Of_Business__r.Name__c=:NewLine) and resume__r.searchable__c = true group by Skill_Level__c order by Skill_Level__c]){
            if((Integer) temp.Get('expr0') > 0){
                Double percent = ((Double) temp.Get('expr0'));
                Pie2.add(new Pie2((String)temp.Get('a'), percent));
            }
        }
        return Pie2;
    }
    
    // Wrapper class
    public class Pie2{
        public String name { get; set; }
        public Double percent { get; set; }
        public Pie2(String name, Double percent) {
            this.name = name;
            this.percent = percent;
        }
    }


// Class to pass save and pass the LOB Name from the Gap analysis page to the NewLOB page

    public PageReference CreateLine() {
        if(NewLOB.Name__c != null){
            insert NewLOB;
            pagereference mypage= new PageReference('/apex/EditLOB?NewLine='+NewLOB.Name__c);
            mypage.setRedirect(true);
            return mypage;
        } else return null;
    }
    
        public PageReference save() {
        if(NewLine != null){
            pagereference mypage= new PageReference('/apex/GapAnalysisReport?NewLine='+NewLine);
            mypage.setRedirect(true);
            return mypage;
        } else return null;
    }
        
    public PageReference ViewLineOfBusiness() {
           String MyLink = getViewLOB();
           if(MyLink != null){
               pagereference mypage= new PageReference('/apex/ViewLine?NewLine='+MyLink);
               mypage.setRedirect(true);
               return mypage;
           } else return null;
    }  
      
    public PageReference DeleteLineOfBusiness() {
           String MyLink = getViewLOB();
           for(LOBSkill__c tempLOBSkill : [Select id from LOBSkill__c Where Line_of_Business__r.Name__c =: MyLink]){
               delete tempLOBSkill;
           }
           for(Line_Of_Business__c tempLOB : [Select id from Line_Of_Business__c where Name__c =: MyLink]){
               delete tempLOB;
           }
           pagereference mypage= new PageReference('/apex/Gap_Analysis');
           mypage.setRedirect(true);
           return mypage;
    }
    
    public PageReference RunReport() {
        String MyLink = getViewLOB();
        if(MyLink != null){
            pagereference mypage= new PageReference('/apex/GapAnalysisReport?NewLine='+MyLink);
            mypage.setRedirect(true);
            return mypage;
        } else return null;
    }
    
        public PageReference AddSkill() { 
        NewLOBSkill.Skills__c = SelectedNewSkillName;
        Line_Of_Business__c tempLOB = [Select id from Line_Of_Business__c Where Name__c =: NewLine];
        NewLOBSkill.Line_Of_Business__c = tempLOB.id;
        insert NewLOBSkill;
        pagereference mypage= new PageReference('/apex/EditLOB?NewLine='+NewLine);
        mypage.setRedirect(true);
        return mypage;
        
    }    
    
        public PageReference DeleteSkill() {
        update getSkills();
        List<LOBSkill__c> deleteList = [SELECT Delete__c, id FROM LOBSkill__c WHERE Delete__c = True];
        for (LOBSkill__c DeleteSkill : deleteList){
            delete DeleteSkill;
        }
        pagereference mypage= new PageReference('/apex/EditLOB?NewLine='+NewLine);
        mypage.setRedirect(true);
        return mypage;
    }
    
        public PageReference DeleteSkill_2() {
        update getSkills();
        List<LOBSkill__c> deleteList = [SELECT Delete__c, id FROM LOBSkill__c WHERE Delete__c = True];
        for (LOBSkill__c DeleteSkill : deleteList){
            delete DeleteSkill;
        }
        pagereference mypage= new PageReference('/apex/ViewLine?NewLine='+NewLine);
        mypage.setRedirect(true);
        return mypage;
    }
    
        public PageReference EditLOB() {
        if(NewLine != null){
            pagereference mypage= new PageReference('/apex/EditLOB?NewLine='+NewLine);
            mypage.setRedirect(true);
            return mypage;
        }else return null;
    }
}